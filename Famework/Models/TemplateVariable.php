<?php
/**
 * Created by PhpStorm.
 * User: michael_keiluweit
 * Date: 18.06.15
 * Time: 10:34
 */

namespace Famework\Models;

use Famework\Interfaces\Model;

class TemplateVariable implements Model
{
    /**
     * @var string
     */
    private $key;

    /**
     * @var mixed
     */
    private $value;

    /**
     * @param string $key
     * @param mixed $value
     */
    public function __construct($key, $value)
    {
        if (empty($key)) {
            throw new \InvalidArgumentException('$key must be set');
        }

        if (!is_string($key)) {
            throw new \InvalidArgumentException('$key must be type of string, "' . gettype($key) . '" was given."');
        }

        $this->key = $key;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
}