<?php
/**
 * Created by PhpStorm.
 * User: mk
 * Date: 23.10.2016
 * Time: 16:16
 */

namespace Famework\Models;


use Famework\Exceptions\Argument\InvalidArgumentException;

class File
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $content = '';

    public function __construct($path)
    {
        if (!file_exists($path)) {
            throw new \RuntimeException('Config file "' . $path . '" was not found!');
        }
        //$this->name = $name; //todo
        $this->path = $path;
    }

    /**
     * @param $fileName
     * @param bool $caseSensitive
     * @return bool
     */
    public static function fileExists($fileName, $caseSensitive = true) {

        if (file_exists($fileName)) {
            return true;
        }

        if ($caseSensitive) {
            return false;
        }

        $directoryName = dirname($fileName);
        $fileArray = glob($directoryName . '/*', GLOB_NOSORT);
        $fileNameLowerCase = ($caseSensitive) ? $fileName : strtolower($fileName);

        foreach ($fileArray as $file) {
            if ($caseSensitive) {
                if ($file == $fileNameLowerCase) {
                    return $file;
                }
            } else {
                if (strtolower($file) == $fileNameLowerCase) {
                    return $file;
                }
            }
        }
        return false;
    }

    /**
     * @return string
     * @throws ConfigException
     */
    public function getContent()
    {
        if ($this->content === '') {
            $this->content = file_get_contents($this->path);
        }
        return $this->content;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
}