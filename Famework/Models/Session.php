<?php
/**
 * Created by PhpStorm.
 * User: michael
 * Date: 20.06.2015
 * Time: 16:42
 */

namespace Famework\Models;

use Famework\Exceptions\Argument\InvalidArgumentException;
use Famework\Exceptions\Session\CanNotStartSessionException;
use Famework\Exceptions\Session\KeyNotFoundException;
use Famework\Exceptions\Session\SessionException;

class Session
{
    const TEMPORARY_MESSAGES = 'temporary_messages';

    private static $instance;

    /**
     * @var string
     */
    protected $sessionId;

    /**
     * The message are stored here and will be moved to $message in the next iteration.
     * @var array
     */
    protected $storedMessages = array();

    /**
     * Must be print in this iteration.
     *
     * @var string[]
     */
    protected $messages = array();

    private function __construct() {}

    private function __clone() {}

    public static function getInstance()
    {
        if (!self::$instance instanceof Session) {
            self::$instance = new Session();
            self::$instance->init();
        }
        return self::$instance;
    }

    /**
     * @throws SessionCanNotStartSessionException
     * @throws SessionException
     */
    protected function init()
    {
        $canStart = @session_start();
        if (!$canStart) {
            throw new CanNotStartSessionException();
        }

        $this->sessionId = session_id();
        if (empty($this->sessionId)) {
            throw new SessionException('Empty session Id');
        }
    }

    public function set($key, $value)
    {
        if (!is_string($key)) {
            throw new InvalidArgumentException($key, '$key', InvalidArgumentException::EXPECTED_TYPE_STRING);
        }
        $_SESSION[$key] = $value;
    }

    /**
     * @param string $key
     * @return string
     * @throws InvalidArgumentException
     * @throws KeyNotFoundException
     */
    public function get($key)
    {
        if (!is_string($key)) {
            throw new InvalidArgumentException($key, '$key', InvalidArgumentException::EXPECTED_TYPE_STRING);
        }

        if (!isset($_SESSION[$key])) {
            throw new KeyNotFoundException();
        }
        return (string) $_SESSION[$key];
    }

    /**
     * @param string $key
     * @throws InvalidArgumentException
     * @throws KeyNotFoundException
     */
    public function delete($key)
    {
        if (!is_string($key)) {
            throw new InvalidArgumentException($key, '$key', InvalidArgumentException::EXPECTED_TYPE_STRING);
        }

        if (!isset($_SESSION[$key])) {
            throw new KeyNotFoundException();
        }
        unset($_SESSION[$key]);
    }

    /**
     * @param string $key
     * @return bool
     * @throws InvalidArgumentException
     */
    public function has($key)
    {
        if (!is_string($key)) {
            throw new InvalidArgumentException($key, '$key', InvalidArgumentException::EXPECTED_TYPE_STRING);
        }

        if (isset($_SESSION[$key])) {
            return true;
        }
        return false;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @return bool
     */
    public function hasTemporaryMessage()
    {
        if (
            isset($_SESSION[Session::TEMPORARY_MESSAGES]) &&
            !empty($_SESSION[Session::TEMPORARY_MESSAGES])
        ) {
            return true;
        }
        return false;
    }

    /**
     * will be replaced by #2
     * @param $message
     */
    public function addTemporaryMessage($message)
    {
        $this->storedMessages[] = $message;
        $_SESSION[Session::TEMPORARY_MESSAGES] = $this->storedMessages;
    }

    /**
     * @return string[]
     */
    public function getTemporaryMessages()
    {
        if ($this->hasTemporaryMessage()) {
            return $_SESSION[Session::TEMPORARY_MESSAGES];
        }
        return [];
    }

    public function deleteTemporaryMessages()
    {
        $this->storedMessages = array();
        $_SESSION[Session::TEMPORARY_MESSAGES] = array();
    }
}