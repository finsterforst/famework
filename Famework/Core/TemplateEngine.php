<?php
/**
 * Created by PhpStorm.
 * User: michael_keiluweit
 * Date: 18.06.15
 * Time: 10:30
 */

namespace Famework\Core;

use Famework\Models\Language;


/**
 * Class TemplateEngine
 * @package oxs\downloadplattform\kernel
 */
class TemplateEngine
{
    const TEMPLATE_EXTENSION = '.twig';
    const TEMPLATE_DIRECTORY = 'App/Views/';

    /**
     * @var TemplateEngine
     */
    private static $instance;

    /**
     * @see http://twig.sensiolabs.org/api/v1.8.1/Twig_Loader_Filesystem.html
     * @var \Twig_Loader_Filesystem
     */
    private $filesystem;

    /**
     * @see http://twig.sensiolabs.org/api/v1.8.1/Twig_Environment.html
     * @var \Twig_Environment
     */
    private $environment;

    /**
     * Contains the rendered template with the variables for it.
     * @var string
     */
    private $renderedTemplate;

    private function __clone() {}

    private function __construct() {}

    /**
     * @return TemplateEngine
     */
    public static function getInstance()
    {
        if (!self::$instance instanceof TemplateEngine) {
            self::$instance = new TemplateEngine();
        }
        return self::$instance;
    }

    /**
     * This methods gives Twig the information, where the templates are stored.
     */
    public function loadFilesystem()
    {
        $this->filesystem = new \Twig_Loader_Filesystem([
            System::getBasePath() .
            self::TEMPLATE_DIRECTORY .
            Router::getInstance()->getControllerName() .
            '/'
        ]);

        /*
         * Because Twig is only checking the path from above, but we are using global templates like layout.twig
         * we must tell Twig, where it can search for other templates, too.
         */
        $this->filesystem->addPath(
            System::getBasePath() .
            self::TEMPLATE_DIRECTORY .
            Router::getInstance()->getControllerDomain() . '/'
        );
    }

    /**
     * The Twig environment is for example needed to execute the render method.
     */
    public function loadEnvironment()
    {
        if ($this->filesystem === null) {
            throw new \RuntimeException();
        }
        $this->environment = new \Twig_Environment($this->filesystem);
        $this->environment->getExtension('core')->setNumberFormat(0, ',', '.'); //@todo put it in app
    }

    /**
     * @todo put it in app
     */
    public function registerExtensions()
    {
        //register shortcut for translation language variable
        /*
        $this->environment->addFunction(new \Twig_SimpleFunction('lang', function ($key, array $params = array()) {
            return Language::getInstance()->get($key);
        }));
        */
    }

    /**
     * Here gets Twig the to render view file and the variables for it from Controller::getVariables().
     */
    public function render()
    {
        if ($this->environment === null) {
            throw new \RuntimeException();
        }

        $tplVariablesAsArray = array();
        foreach (FrontController::getInstance()->getController()->getTemplateVariables() as $tplVarObj) {
            $tplVariablesAsArray[$tplVarObj->getKey()] = $tplVarObj->getValue();
        }

        $this->renderedTemplate = $this->environment->render(
            Router::getInstance()->getViewName() . self::TEMPLATE_EXTENSION,
            $tplVariablesAsArray
        );
    }

    /**
     * Displays the rendered template via the php call echo.
     */
    public function display()
    {
        echo $this->renderedTemplate;
    }
}

