<?php

namespace Famework\Core;

class System
{
    /**
     * @var System
     */
    private static $instance;

    private function __construct() {}

    private function __clone() {}

    /**
     * @return System
     */
    public static function getInstance()
    {
        if (!self::$instance instanceof System) {
            self::$instance = new System();
        }
        return self::$instance;
    }

    public function init()
    {
        $this->initAutoloader();
        $this->initConfig();

        $this->initTimeZone();
        $this->initRouter();
        $this->initFrontController();
        $this->initTemplateEngine();
    }

    public function initAutoloader()
    {
        $pathToAutoloader = self::getBasePath() . 'Famework/Core/Autoloader.php';

        if (!file_exists($pathToAutoloader)) {
            throw new \Exception('Error during booting routine (#1). Affected file: "'.$pathToAutoloader.'"');
        }

        require_once $pathToAutoloader;

        if (!class_exists('Famework\Core\Autoloader')) {
            throw new \Exception('Error during booting routine (#2).');
        }

        $autoloader = new Autoloader('Famework');
        $autoloader->register();
    }

    public function initConfig()
    {
        Config::getInstance()->init();
    }

    public function initTimeZone()
    {
        date_default_timezone_set(Config::getInstance()->get('timezone'));
    }

    /**
     * execute all needed methods of the router.
     */
    private function initRouter()
    {
        Router::getInstance()->init();
    }

    private function initFrontController()
    {
        FrontController::getInstance()->init();
        FrontController::getInstance()->process();
    }

    /**
     * execute all needed methods of the template engine.
     */
    private function initTemplateEngine()
    {
        TemplateEngine::getInstance()->loadFilesystem();
        TemplateEngine::getInstance()->loadEnvironment();
        TemplateEngine::getInstance()->registerExtensions();
        TemplateEngine::getInstance()->render();
        TemplateEngine::getInstance()->display();
    }

    /**
     * @return string
     */
    public static function getBasePath()
    {
        return __DIR__ . '/../../';
    }
}