<?php
/**
 * Created by PhpStorm.
 * User: michael_keiluweit
 * Date: 18.06.15
 * Time: 09:59
 */

namespace Famework\Core;

use Famework\Exceptions\Argument\InvalidArgumentException;
use Famework\Exceptions\RouterErrorWhileDecodingUrlException;
use Famework\Models\File;


/**
 * Class Router
 */
class Router
{
    const STORAGE_TYPE_POST = 0;
    const STORAGE_TYPE_GET = 1;

    /**
     * @var Router
     */
    private static $instance;

    /**
     * Contains the current URL as an array.
     * The format is like:
     * URL: /overview/index/param1/param2/
     * $uri: array (
     *     'overview' => 'index',
     *     'param1' => 'param2',
     * )
     *
     * @var array
     */
    private $uri = array();

    /**
     * Contains the get parameter from the url. instead of /id/1 it contains array('id' => '1')
     *
     * @var string[]
     */
    private $get = array();

    /**
     * Contains the post parameter of a form.
     *
     * @var string[]
     */
    private $post = array();

    /**
     * Name of the folder from which the controller should be loaded.
     * The direcotory names are:
     * - CONTROLLER_DOMAIN_ADMIN
     * - CONTROLLER_DOMAIN_FRONTEND
     * - CONTROLLER_DOMAIN_GAME
     *
     * @var string
     */
    private $controllerDomain;

    /**
     * Only name of controller, not the controller object itself.
     *
     * @var string
     */
    private $controllerName;

    /**
     * The full controller namespace path. Build in Router::buildControllerNamespacePath().
     * Example: \cygnet\controller\frontend\Login
     *
     * @var string
     */
    private $controllerNamespacePath;

    /**
     * Name of the to calling view
     *
     * @var string
     */
    private $viewName;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    /**
     * @return Router
     */
    public static function getInstance()
    {
        if (!self::$instance instanceof Router) {
            self::$instance = new Router();
        }
        return self::$instance;
    }

    /**
     * Calls all needed methods to use the other public methods.
     */
    public function init()
    {
        $this->readRoutingPath();
        $this->readParametersFromUrl();
        $this->readParametersFromPost();
        $this->buildControllerNamespacePath();
    }

    /**
     * Checks and gets the controller and view parameter from the url. If both or the view parameter are missing, it is
     * replaced by default parameters. For controller it is "Start" and for the view it is "index". So if some of
     * them are missed, the framework will forward the user to the login page.
     */
    private function readRoutingPath()
    {
        $controller = 'Start';
        $view = 'index';

        if (
            !array_key_exists('PATH_INFO', $_SERVER) ||
            empty($_SERVER['PATH_INFO']) ||
            $_SERVER['PATH_INFO'] == '/'
        ) {
            $uri = '/';
        } else {
            $uri = $_SERVER['PATH_INFO'];
        }

        /*
         * We can get a url like /overview/index/ . With 3 slashes, we have an array with 3 entries which is bad.
         * In reality we have just one slash (overview/index), so wie trim the first and the last slash.
         */
        if ($uri != '/') {
            $uri = trim($uri, '/');
        }

        $this->uri = explode('/', $uri);

        if (isset($this->uri[0]) && !empty($this->uri[0])) {
            $controller = $this->uri[0];
        }

        if (isset($this->uri[1]) && !empty($this->uri[1])) {
            $view = $this->uri[1];
        }

        $this->controllerName = lcfirst($controller);
        $this->viewName = lcfirst($view);
    }

    /**
     * Therefore the htaccess is rewriting the URL, the framework can't access parameters via $_GET.
     * The variable Router::$uri contains the url as it is: /home/register/key/value/...
     * But we need the parameters like as we would get it from $_GET. So this method builds an array like $_GET.
     *
     * @throws \RuntimeException
     */
    private function readParametersFromUrl()
    {
        $count = count($this->uri);

        if (count($this->uri) % 2 !== 0 && $this->uri[0] != 'robots.txt') {
            throw new RouterErrorWhileDecodingUrlException('error while decoding URL: ' . json_encode($this->uri));
        }

        for ($i = 0; $i < $count; $i++) {
            $this->get[$this->uri[$i]] = $this->uri[++$i];
        }
    }
//
    /**
     * This function must be called after Router::readParametersFromUrl().
     */
    private function readParametersFromPost()
    {
        foreach ($_POST as $key => $value) {
            $this->post[$key] = $value;
        }
    }

    /**
     * Builds the full namespace path for the controller.
     * Example Router::$controllerNamespacePath
     *
     * It is needed for the autoloader there it is able to load dynamically classes, especially controller.
     *
     * @TODO build static array with all classes as it will increase the performance and changes aren't sequential.
     * @TODO check the app config which controller is the start point, if no routing information available, like /.
     * @TODO if no app config, show Famework Example Controller.
     */
    public function buildControllerNamespacePath()
    {
        $baseControllerPath = System::getBasePath() . 'App/Controller/';


            if (File::fileExists($baseControllerPath . $this->getControllerName() . '.php', false)) {
                $this->controllerNamespacePath = '\App\Controller' . '\\' . $this->getControllerName();
                $this->controllerDomain = null; //@todo deleteme
            }

    }

    /**
     * Redirects the user to the given controller. It executes an exit just behind the header function to stop the
     * script and its processes after it.
     *
     * @todo exception
     *
     * @param string $controller
     * @param string $view
     */
    public function redirect($controller, $view = 'index')
    {
        header('Location: /' . $controller . '/' . $view);
        exit;
    }

    /**
     * @param string $key
     * @param int $storageType
     * @return string
     * @throws InvalidArgumentException
     */
    public function get($key, $storageType = Router::STORAGE_TYPE_POST)
    {
        if (!is_string($key)) {
            throw new InvalidArgumentException($key, '$key', InvalidArgumentException::EXPECTED_TYPE_STRING);
        }

        if (!is_int($storageType)) {
            throw new InvalidArgumentException($storageType, '$storageType', InvalidArgumentException::EXPECTED_TYPE_INTEGER);
        }

        if ($storageType == Router::STORAGE_TYPE_POST) {
            $storage = $this->post;
        } else {
            $storage = $this->get;
        }

        if ($this->has($key, $storageType)) {
            return $storage[$key];
        }
        return '';
    }

    /**
     * @param string $key
     * @param int $storageType
     * @return string
     * @throws InvalidArgumentException
     */
    public function has($key, $storageType = Router::STORAGE_TYPE_POST)
    {
        if (!is_string($key)) {
            throw new InvalidArgumentException($key, '$key', InvalidArgumentException::EXPECTED_TYPE_STRING);
        }

        if (!is_int($storageType)) {
            throw new InvalidArgumentException($storageType, '$storageType', InvalidArgumentException::EXPECTED_TYPE_INTEGER);
        }

        if ($storageType == Router::STORAGE_TYPE_POST) {
            $storage = $this->post;
        } else {
            $storage = $this->get;
        }

        if (isset($storage[$key])) {
            return true;
        }
        return false;
    }

    /**
     * @param $key

     * @param $value
     * @throws InvalidArgumentException
     * @todo desc
     */
    public function setPost($key, $value)
    {
        if (!is_string($key)) {
            throw new InvalidArgumentException($key, '$key', InvalidArgumentException::EXPECTED_TYPE_STRING);
        }
        $this->post[$key] = $value;
    }

    /**
     * Nearly the same method as FrontController::has() but returns also false if the value is empty
     * @see http://php.net/manual/de/function.empty.php
     *
     * @param string $key
     * @param int $storageType
     * @return string
     * @throws InvalidArgumentException
     */
    public function hasAndNotEmpty($key, $storageType = Router::STORAGE_TYPE_POST)
    {
        if (!is_string($key)) {
            throw new InvalidArgumentException($key, '$key', InvalidArgumentException::EXPECTED_TYPE_STRING);
        }

        if (!is_int($storageType)) {
            throw new InvalidArgumentException($storageType, '$storageType', InvalidArgumentException::EXPECTED_TYPE_INTEGER);
        }

        if ($this->has($key, $storageType)) {
            $value = $this->get($key, $storageType);
            if (!empty($value)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return string
     */
    public function getControllerNamespacePath()
    {
        return $this->controllerNamespacePath;
    }

    /**
     * @return string
     */
    public function getControllerName()
    {
        return ucfirst($this->controllerName);
    }

    /**
     * @return string
     */
    public function getViewName()
    {
        return $this->viewName;
    }

    /**
     * @return array
     */
    public function getGet()
    {
        return $this->get;
    }

    /**
     * @return array
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @return string
     */
    public function getControllerDomain()
    {
        return $this->controllerDomain;
    }
}
