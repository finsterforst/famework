<?php

namespace Famework\Core;

class Autoloader
{
    /**
     * @var string
     */
    private $namespace;

    /**
     * @param string $namespace
     */
    public function __construct($namespace = '')
    {
        $this->namespace = $namespace;
    }

    /**
     * Calls the PHP function spl_autoload_register to register itself as an autoloader.
     */
    public function register()
    {
        spl_autoload_register(array($this, 'loadClass'));
    }

    /**
     * @param string $className
     */
    public function loadClass($className)
    {
        if($this->namespace !== '') {
            //$className = str_replace($this->namespace . '\\', '', $className);
        }
        $className = str_replace('\\', DIRECTORY_SEPARATOR, $className);
        $file = System::getBasePath() . $className. '.php';

        if(file_exists($file)) {
            require_once $file;
        }
    }
}