<?php
/**
 * Created by PhpStorm.
 * User: michael_keiluweit
 * Date: 18.06.15
 * Time: 09:36
 */

namespace Famework\Core;

use Famework\Interfaces\Controller;
use Famework\Exceptions\Controller\UnknownControllerException;
use Famework\Services\Logger;

/**
 * Class FrontController
 * @package oxs\downloadplattform\kernel
 */
class FrontController
{
    /**
     * @var FrontController
     */
    private static $instance;

    /**
     * @var string
     */
    private $controllerNamespacePath;

    /**
     * @var Controller
     */
    private $controller;

    /**
     * @var string[]
     */
    private $get = array();

    /**
     * @var string[]
     */
    private $post = array();

    private function __construct() {}

    private function __clone() {}

    /**
     * @return FrontController
     */
    public static function getInstance()
    {
        if (!self::$instance instanceof FrontController) {
            self::$instance = new FrontController();
        }
        return self::$instance;
    }

    /**
     * Calls all needed methods to use the other public methods.
     */
    public function init()
    {
        $this->restoreGet();
        $this->restorePost();
        $this->controllerNamespacePath = Router::getInstance()->getControllerNamespacePath();
    }

    private function restoreGet()
    {
        $this->get = $_GET;
        //$_GET = array();
    }

    private function restorePost()
    {
        $this->post = $_POST;
        //$_POST = array();
    }

    /**
     * This method is the central place where the controller and its view method is called. With the needed
     * url parameters given by the Router, we can execute the method here. Therefore the pre hook method
     * Controller::onLoad() is also called here, before the view method is executed.
     * In case something is going wrong from the application side, here are the errors are caught. Global exceptions
     * like InvalidArgument and RuntimeException are caught directly in index.php.
     */
    public function process()
    {
        $this->logExecute();

        try {
            $this->controller = $this->buildControllerObject();
            $methodName = $this->readMethodName($this->controller);

            $this->controller->onLoad();
            $this->controller->$methodName();
            $this->controller->onUnload();
        } catch (\Exception $ex) {
            print_r($ex);
        }
    }

    /**
     * With the already known namespace path of the to calling controller a controller object is instantiated.
     * @return Controller
     * @throws UnknownControllerException
     */
    private function buildControllerObject()
    {
        if (!class_exists($this->controllerNamespacePath)) {
            throw new UnknownControllerException();
        }

        /** @var Controller $controller */
        $controller = new $this->controllerNamespacePath();

        if (!$controller instanceof \Famework\Interfaces\Controller) {
            throw new \RuntimeException('given object is not a controller ('.$this->controllerNamespacePath.')');
        }
        return $controller;
    }

    /**
     * Gets the method name from the Router and checks if the already build controller object has the method.
     *
     * @param Controller $controller
     * @return string
     * @throws \RuntimeException
     */
    private function readMethodName(Controller $controller)
    {
        $methodName = Router::getInstance()->getViewName() . 'Action';
        if (!method_exists($controller, $methodName)) {
            throw new \RuntimeException('No method "'.$methodName.'" for controller "'.Router::getInstance()->getControllerName().'"');
        }
        return $methodName;
    }

    /**
     * Collects all needed data and format them. After that it calls the Logger method Logger:log().
     */
    private function logExecute()
    {
        $parameter = implode(',', Router::getInstance()->getUri());
        $post = http_build_query($_POST);
        $get = http_build_query($_GET);

        //else we get just a " , ".
        if (strlen($parameter) == 1) {
            $parameter = '';
        }

        Logger::getInstance()->log(
            (
                'ROUTING: ' .
                Router::getInstance()->getControllerDomain() . '/'.
                Router::getInstance()->getControllerName() . '/'.
                Router::getInstance()->getViewName().'/ - ' .
                'PARAMETER: '. $parameter . ' - '.
                'post: ' . $post . ' - '.
                'get: ' . $get
            ),
            Logger::DEFAULT_ROUTING_LOG
        );
    }

    /**
     * @return Controller
     */
    public function getController()
    {
        return $this->controller;
    }
}