<?php

namespace Famework\Core;

use Famework\Exceptions\Config\ConfigException;
use Famework\Models\File;
use Symfony\Component\Yaml\Parser;

class Config
{
    /**
     * @var Config
     */
    private static $instance;

    /**
     * @var string[]
     */
    private $fameworkConfigAsArray = array();

    private function __construct() {}

    private function __clone() {}

    /**
     * @return Config
     */
    public static function getInstance()
    {
        if (!self::$instance instanceof Config) {
            self::$instance = new Config();
        }
        return self::$instance;
    }

    /**
     * First it reads the configuration from the Famework folder. After that it overwrites the configuration by the
     * App folder.
     */
    public static function init()
    {
        $famework = new File(System::getBasePath() . 'Famework/config/parameters.yaml');
        $yamlParser = new Parser();
        self::$instance->fameworkConfigAsArray = $yamlParser->parse($famework->getContent());

        try {
            $app = new File(System::getBasePath() . 'App/config/parameters.yaml');
            self::$instance->fameworkConfigAsArray =
                array_replace_recursive(
                    self::$instance->fameworkConfigAsArray,
                    $yamlParser->parse($app->getContent())
                )
            ;
        } catch (\RuntimeException $ex) {
        }
    }

    /**
     * @param string $section
     * @return string[]
     * @throws ConfigException
     */
    public function get($section = '')
    {
        if ($section !== '') {
            if (!array_key_exists($section, $this->fameworkConfigAsArray)) {
                throw new ConfigException('Section "'. $section .'" does not exist.');
            }
            return $this->fameworkConfigAsArray[$section];
        }
        return $this->fameworkConfigAsArray;
    }
}