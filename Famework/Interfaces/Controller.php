<?php

namespace Famework\Interfaces;
use Famework\Models\TemplateVariable;

interface Controller
{
    /**
     *
     */
    public function onLoad();

    /**
     *
     */
    public function onUnload();

    /**
     * @return TemplateVariable[]
     */
    public function getTemplateVariables();
}