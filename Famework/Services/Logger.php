<?php
/**
 * Created by PhpStorm.
 * User: michael_keiluweit
 * Date: 18.06.15
 * Time: 10:23
 */

namespace Famework\Services;

use Famework\Core\System;

/**
 * Class Logger
 *
 * @todo split me up / make me more generous.
 */
class Logger
{
    const DEFAULT_ERROR_LOG = 'error';
    const DEFAULT_ROUTING_LOG = 'routing';
    //const DEFAULT_DOWNLOAD_LOG = 'download';
    const LOG_FILE_EXTENSION = '.log';
    const DEFAULT_QUERY_LOG = 'query';

    private $logFolder = 'logs/';

    /**
     * @var Logger
     */
    private static $instance;

    private function __construct() {}

    private function __clone() {}

    /**
     * @return Logger
     */
    public static function getInstance()
    {
        if (!self::$instance instanceof Logger) {
            self::$instance = new Logger();
            self::$instance->logFolder = self::$instance->readLogFolder();
        }
        return self::$instance;
    }

    /**
     * Writes the log file (and creates is, when it is not there) with the PHP function error_log.
     * It appends the string "[end]" as the end of the line to be sure, that the log text is complete.
     * Use for destinationFile the class constants, except you need another one for temporary using.
     *
     * @param string $message
     * @param string $destinationFile
     */
    public function log($message, $destinationFile)
    {
        $log =
            $this->buildDate() .
            ' - ' .
            $message .
            ' [end]' .
            PHP_EOL
        ;
        error_log(
            $log,
            3,
            $this->logFolder . $destinationFile . Logger::LOG_FILE_EXTENSION
        );
    }

    /**
     * Returns date format like:
     * YYYY/MM/DD - HH:MM:SS
     *
     * @return string
     */
    private function buildDate()
    {
        return date('Y/m/d - H:i:s', time());
    }

    /**
     * Read and returns the log destination folder from the config file global.ini (log_folder).
     *
     * @return string
     * @throws \RuntimeException
     */
    private function readLogFolder()
    {
        /*
        $config = new ConfigHandler('global');
        $ini = $config->getConfigFile();

        if (!isset($ini['log_folder'])) {
            throw new \RuntimeException('configuration variable "log_folder" is not defined!');
        }
        return $ini['log_folder'];
        */
        return System::getBasePath() . $this->logFolder;
    }
}