<?php
/**
 * Created by PhpStorm.
 * User: michael keiluweit
 * Date: 08.07.14
 * Time: 11:26
 */

namespace Famework\Services;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Famework\Core\Config;
use Famework\Core\System;
use Famework\Exceptions\Database\DatabaseNoConnectionException;

class Database
{
    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * @var Database
     */
    private static $instance;

    /**
     * @var EntityManager
     */
    private $entityManager;

    private function __construct() {}

    private function __clone() {}

    /**
     * return instance and init connection
     *
     * @return Database
     * @throws ConfigurationPartNotFoundException
     * @throws DatabaseException
     * @throws DatabaseNoConnectionException
     * @throws InvalidArgumentException
     */
    public static function getInstance()
    {
        if (!self::$instance instanceof Database) {
            self::$instance = new Database();

            //$configDbParams = System::getInstance()->getConfig()['database'];
            $configDbParams = Config::getInstance()->get('database');
            $expectedParameters = array('driver', 'host', 'port', 'dbname', 'user', 'password');

            foreach ($expectedParameters as $param) {
                if (!array_key_exists($param, $configDbParams)) {
                    throw new DatabaseNoConnectionException('Missing parameter "'.$param.'". Check your config file.');
                }
            }

            $config = Setup::createAnnotationMetadataConfiguration(
                [System::getBasePath() . 'App/Models/'],
                true
            );
            self::$instance->entityManager = EntityManager::create($configDbParams, $config);
        }
        return self::$instance;
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }
}