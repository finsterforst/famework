<?php

namespace Famework\Services;

use Famework\Core\System;
use Famework\Exceptions\Config\ConfigHandlerTypeIsNotSupportedException;
use Symfony\Component\Yaml\Parser;

/**
 * Class ConfigHandler
 *
 */
class ConfigFileHandler
{
    /**
     * Extension for YAML files.
     * This is the default value for the constructor.
     *
     * @var string
     */
    const CONFIGURATION_EXTENSION_INI = '.ini';

    /**
     * Extension for YAML files.
     *
     * @var string
     */
    const CONFIGURATION_EXTENSION_YAML = '.yaml';

    /**
     * Extension for JSON files.
     *
     * @var string
     */
    const CONFIGURATION_EXTENSION_JSON = '.json';

    /**
     * Contains the full path to the configuration file.
     *
     * @var string
     */
    private $fullPathToConfigurationFile = '';

    /**
     * Holds the configuration keys and its values.
     *
     * @var array
     */
    private $configFile;

    /**
     * The name of the file.
     *
     * @var string
     */
    private $file;

    /**
     * The extension of the config file. Usually it is ".ini".
     *
     * @var string
     */
    private $extension;

    /**
     * All needed class variables are setted here to work with the other public methods.
     *
     * @param string $file
     * @param string $ext
     * @throws \RuntimeException
     * @throws \InvalidArgumentException
     */
    public function __construct($file, $ext = ConfigFileHandler::CONFIGURATION_EXTENSION_YAML)
    {
        if (!is_string($file)) {
            throw new \InvalidArgumentException();
        }

        if (!is_string($ext)) {
            throw new \InvalidArgumentException();
        }

        $this->file = $file;
        $this->extension = $ext;

        $this->buildFullPathToConfigurationFile();
        $this->loadConfigurationFile();
    }

    /**
     * builds the full path to the configuration file, using System::getBasePath, the configuration folder (constructor),
     * the file name and the file extension.
     * It stores the value in ConfigHandler::$fullPathToConfigurationFile.
     */
    private function buildFullPathToConfigurationFile()
    {
        $this->fullPathToConfigurationFile =
            System::getBasePath() .
            'App/config/' .
            $this->file .
            $this->extension
        ;
    }

    /**
     * @throws \RuntimeException
     */
    private function loadConfigurationFile()
    {
        if (!file_exists($this->fullPathToConfigurationFile)) {
            throw new \RuntimeException('can not load file ' . $this->file . ' from path: ' . $this->fullPathToConfigurationFile . '!');
        }

        switch ($this->extension) {
            case self::CONFIGURATION_EXTENSION_INI: $this->loadConfigurationFileIni(); break;
            case self::CONFIGURATION_EXTENSION_YAML: $this->loadConfigurationFileYaml(); break;
            case self::CONFIGURATION_EXTENSION_JSON: $this->loadConfigurationFileJSON(); break;
            default: throw new ConfigHandlerTypeIsNotSupportedException('The type "' . $this->extension . '" is not supported.');
        }
    }

    /**
     * Interpreting the configuration file with the PHP function parse_ini_file.
     *
     * @link http://php.net/manual/en/function.parse-ini-file.php
     * @throws \RuntimeException
     */
    private function loadConfigurationFileIni()
    {
        if (!file_exists($this->fullPathToConfigurationFile)) {
            throw new \RuntimeException('can not load file ' . $this->file . ' from path: ' . $this->fullPathToConfigurationFile . '!');
        }

        if ($this->extension !== '.ini') {
            throw new \RuntimeException('Cannot load the configuration file "'.$this->file.'" with the extension "'.$this->extension.'".');
        }

        $this->configFile = parse_ini_file($this->fullPathToConfigurationFile, true);
    }

    /**
     * Interpreting the configuration file with the Symfony component Yaml 2.
     *
     * @link http://symfony.com/components/Yaml
     * @throws \RuntimeException
     */
    private function loadConfigurationFileYaml()
    {
        if ($this->extension !== '.yaml') {
            throw new \RuntimeException('Cannot load the configuration file "'.$this->file.'" with the extension "'.$this->extension.'".');
        }

        $file = new File($this->fullPathToConfigurationFile);
        $yamlParser = new Parser();
        $this->configFile = $yamlParser->parse($file->getContent());
    }

    /**
     * Interpreting the configuration file with the Symfony component Yaml 2.
     *
     * @link http://symfony.com/components/Yaml
     * @throws \RuntimeException
     */
    private function loadConfigurationFileJSON()
    {
        if ($this->extension !== '.json') {
            throw new \RuntimeException('Cannot load the configuration file "'.$this->file.'" with the extension "'.$this->extension.'".');
        }

        $file = new File($this->fullPathToConfigurationFile);
        $this->configFile = json_decode($this->getFileContents($file->getContent()));
    }

    /**
     * @return array
     */
    public function getConfigFile()
    {
        return $this->configFile;
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }
}
