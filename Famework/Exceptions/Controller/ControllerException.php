<?php
/**
 * Created by PhpStorm.
 * User: mk
 * Date: 23.10.2016
 * Time: 12:36
 */

namespace Famework\Exceptions\Controller;

use Famework\Exceptions\FameworkException;

class ControllerException extends FameworkException  {}