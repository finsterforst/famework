<?php
/**
 * Created by PhpStorm.
 * User: michael_keiluweit
 * Date: 18.06.15
 * Time: 10:04
 */

namespace Famework\Exceptions\Controller;

/**
 * Class UnknownControllerException
 * @package cygnet\exceptions
 * @todo naming
 */
class UnknownControllerException extends ControllerException {}