<?php

namespace Famework\Exceptions\Session;

class CanNotStartSessionException extends SessionException {}