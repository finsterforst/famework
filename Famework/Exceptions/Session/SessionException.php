<?php

namespace Famework\Exceptions\Session;

use Famework\Exceptions\FameworkException;

class SessionException extends FameworkException  {}