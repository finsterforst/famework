<?php

namespace Famework\Exceptions\Session;

class KeyNotFoundException extends SessionException {}