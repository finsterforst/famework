<?php
/**
 * Created by PhpStorm.
 * User: mk
 * Date: 24.10.2016
 * Time: 08:37
 */

namespace Famework\Exceptions\Registry;


use Famework\Exceptions\FameworkException;

class ObjectIsNotRegisterableException extends FameworkException {}