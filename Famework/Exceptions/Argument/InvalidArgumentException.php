<?php
/**
 * Created by PhpStorm.
 * User: michael
 * Date: 20.06.2015
 * Time: 16:44
 */

namespace Famework\Exceptions\Argument;


use Famework\Exceptions\FameworkException;

class InvalidArgumentException extends FameworkException
{
    const EXPECTED_TYPE_STRING = 'string';
    const EXPECTED_TYPE_INTEGER = 'integer';
    const EXPECTED_TYPE_ARRAY = 'array';
    const EXPECTED_TYPE_BOOLEAN = 'boolean';
    const EXPECTED_TYPE_OBJECT = 'object';
    const EXPECTED_TYPE_FLOAT = 'float';
    const EXPECTED_TYPE_CUSTOM = 'custom';

    /**
     * @param string $parameter
     * @param string $parameterName
     * @param string $expectedType
     * @param string $customExpectedType
     */
    public function __construct($parameter, $parameterName, $expectedType, $customExpectedType = '')
    {
        if (
            (
                $expectedType == self::EXPECTED_TYPE_OBJECT ||
                $expectedType == self::EXPECTED_TYPE_CUSTOM
            ) &&
            $customExpectedType !== ''
        ) {
            $expectedType = $customExpectedType;
        }

        $message = 'The parameter "' . $parameterName . '" must be type of ' . $expectedType . ', but got ' . gettype($parameter) . '.';

        parent::__construct($message);
    }
}