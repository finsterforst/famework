<?php
/**
 * Created by PhpStorm.
 * User: michael_keiluweit
 * Date: 18.06.15
 * Time: 10:07
 */

namespace Famework\Exceptions;

class RouterErrorWhileDecodingUrlException extends RouterException {}