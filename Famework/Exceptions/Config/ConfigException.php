<?php

namespace Famework\Exceptions\Config;
use Famework\Exceptions\FameworkException;

/**
 * Created by PhpStorm.
 * User: mk
 * Date: 23.10.2016
 * Time: 12:23
 */
class ConfigException extends FameworkException {}