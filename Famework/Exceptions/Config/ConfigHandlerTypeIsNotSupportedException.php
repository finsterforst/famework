<?php
/**
 * Created by PhpStorm.
 * User: michael
 * Date: 21.06.2015
 * Time: 22:15
 */

namespace Famework\Exceptions\Config;

class ConfigHandlerTypeIsNotSupportedException extends ConfigException {}