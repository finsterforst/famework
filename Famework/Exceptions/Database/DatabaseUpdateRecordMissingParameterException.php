<?php
/**
 * Created by PhpStorm.
 * User: michael_keiluweit
 * Date: 28.07.15
 * Time: 11:35
 */

namespace Famework\Exceptions\Database;


class DatabaseUpdateRecordMissingParameterException extends DatabaseException {}