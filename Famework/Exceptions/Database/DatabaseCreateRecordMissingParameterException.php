<?php
/**
 * Created by PhpStorm.
 * User: michael
 * Date: 20.06.2015
 * Time: 20:00
 */

namespace Famework\Exceptions\Database;


class DatabaseCreateRecordMissingParameterException extends DatabaseException {}