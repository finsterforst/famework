<?php
/**
 * Created by PhpStorm.
 * User: michael
 * Date: 20.06.2015
 * Time: 19:24
 */

namespace Famework\Exceptions\Database;

use Famework\Exceptions\FameworkException;

class DatabaseException extends FameworkException {}