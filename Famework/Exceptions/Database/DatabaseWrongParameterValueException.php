<?php
/**
 * Created by PhpStorm.
 * User: michael_keiluweit
 * Date: 23.06.15
 * Time: 08:08
 */

namespace Famework\Exceptions\Database;

class DatabaseWrongParameterValueException extends DatabaseException
{
    /**
     * $mustBe is an array of all possible values. Example for one entry of the array could be "1-9", 1, > 2, false, ...
     *
     * @param string $actual
     * @param string $variableName
     * @param array $mustBe
     */
    public function __construct($actual, $variableName, array $mustBe)
    {
        $message = '
            Wrong value for parameter "' . $variableName . '".
            It has the value: "'.$actual.'", but must be: '.implode(',', $mustBe) . '.
        ';

        parent::__construct($message);
    }
}