<?php
/**
 * Created by PhpStorm.
 * User: michael
 * Date: 20.06.2015
 * Time: 19:55
 */

namespace Famework\Exceptions\Database;


class DatabaseNoIdException extends DatabaseException {}