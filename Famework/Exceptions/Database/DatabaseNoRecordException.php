<?php
/**
 * Created by PhpStorm.
 * User: michael
 * Date: 20.06.2015
 * Time: 19:48
 */

namespace Famework\Exceptions\Database;

class DatabaseNoRecordException extends DatabaseException
{
    /**
     * @param int $id
     * @param int $sql
     */
    public function __construct($id, $sql)
    {
        parent::__construct('no record for id "' . $id . '", query: - ' .  $sql);
    }
}