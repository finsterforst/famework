<?php
/**
 * Created by PhpStorm.
 * User: michael
 * Date: 04.07.18
 * Time: 09:20
 */

namespace Famework;

$rootDir = __DIR__ . '/';
$pathToVendorAutoloader = $rootDir . 'libs/vendor/autoload.php';
$pathToSystemClass = $rootDir . 'Famework/Core/System.php';

if (!file_exists($pathToVendorAutoloader)) {
    throw new \Exception('Can\'t initialize the libraries.');
}

if (!file_exists($pathToSystemClass)) {
    throw new \Exception('Can\'t initialize.');
}

require_once $pathToVendorAutoloader;
require_once $pathToSystemClass;