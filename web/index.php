<?php

namespace Famework\web;

use Famework\Core\System;

require_once '../bootstrap.php';

try {
    System::getInstance()->init();
} catch (\Exception $ex) {
    echo '<pre>';
    print_r($ex);
    echo '</pre>';
}