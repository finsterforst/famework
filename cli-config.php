<?php
/**
 * Created by PhpStorm.
 * User: michael
 * Date: 04.07.18
 * Time: 09:20
 */


// cli-config.php
require_once 'bootstrap.php';

\Famework\Core\System::getInstance()->initAutoloader();
\Famework\Core\System::getInstance()->initConfig();
\Famework\Core\System::getInstance()->initTimeZone();

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet(
    \Famework\Services\Database::getInstance()->getEntityManager()
);
